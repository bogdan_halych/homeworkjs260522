// Выполнить запрос на https://swapi.dev/api/people получить список героев звездных войн.

//             Вывести каждого героя отдельной карточкой с указанием. Имени, половой принадлежности, рост, цвет кожи, 
//             год рождения и планету на которой родился.
            
//             Создайте кнопку сохранить на каждой карточке. При нажатии кнопки записшите информацию в браузере
            
//             Возможные свойства  :
//             name строка - Имя этого человека.
//             birth_year строка - Год рождения человека в соответствии со вселенскими стандартами ДБЯ или ПБЯ - до битвы при Явине или после битвы при Явине. Битва при Явине - это битва, которая происходит в конце эпизода IV «Звездных войн»: Новая надежда.
//             eye_color строка - Цвет глаз этого человека. Будет "неизвестно", если неизвестно, или "н / д", если у человека нет глаза.
//             gender строка - Пол этого человека. Либо «Мужской», «Женский», либо «Неизвестный», «н / д», если у человека нет пола.
//             hair_color строка - Цвет волос этого человека. Будет "неизвестно", если неизвестно, или "н / п", если у человека нет волос.
//             height строка - Рост человека в сантиметрах.
//             mass строка - Масса человека в килограммах.
//             skin_color строка - Цвет кожи этого человека.
//             homeworld строка - URL ресурса планеты, планеты, на которой этот человек родился или населяет.
//             films array - массив URL-адресов киноресурсов, в которых был этот человек.
//             species array - массив URL-адресов ресурсов видов, к которым принадлежит этот человек.
//             starships array - Массив URL-адресов ресурсов звездолета, которые пилотировал этот человек.
//             vehicles array - массив URL-адресов ресурсов транспортного средства, которые пилотировал этот человек.
//             url строка - URL-адрес гипермедиа этого ресурса.
//             created строка - формат даты ISO 8601 времени создания этого ресурса.
//             edited строка - формат даты ISO 8601 времени, когда этот ресурс редактировался.
            
//             Для создания карточек испозуйте классы



let url = "https://swapi.dev/api/people";

localStorage.cartData = JSON.stringify([]); //Масив на LocalStorage
let cartArray = []; // Масив на сторінці(зникне після перезавантаження)
let peoples = fetch(url);

peoples
  .then((res) => res.json())
  .then((peoplesArr) => peoplesArr.results)
  .then((peoplesArrObj) => {
    hide();
    peoplesArrObj.forEach((element) => {
      let cart = new PeopleCart(
        element.name,
        element.gender,
        element.height,
        element.skin_color,
        element.birth_year,
        element.homeworld
      );
      cart.showCart();
      cartArray.push(cart);
    });
  });

//Створення класу для формування карток
class PeopleCart {
  constructor(name, gender, height, skin_color, birth_year, homeworld) {
    this.name = name;
    this.gender = gender;
    this.height = height;
    this.skin_color = skin_color;
    this.birth_year = birth_year;
    this.homeworld = homeworld;
  }

  //Функція виводу карток.
  showCart() {
    let div = document.createElement("div");
    let a = document.createElement("a");
    for (let data of Object.entries(this)) {
      let inf = document.createElement("div");
      a = document.createElement("a");
      inf.innerText = data[0] + ": " + data[1];
      if(data[1].includes("http")) {
        a.setAttribute("href", data[1]);
        a.innerText = data[1];
        continue;
      }
      div.append(inf);
      document.querySelector(".people-cart-box").append(div);
    }
    div.append(a);
    let btn = document.createElement("input");
    btn.setAttribute("data-name", this.name); //Атрибут для знаходження об'єкта кнопки
    btn.type = "button";
    btn.value = "Save";
    btn.classList.add("btn");
    div.append(btn);
  }
}

//Зберігаємо в localStorage.cartData
document.querySelector(".people-cart-box").onclick = (e) => {
  let input = e.target;
  if (input.tagName != "INPUT") return;
  let data = input.dataset.name;
  console.log(data);
  cartArray.forEach((el) => {
    if (el.name === data) {
      let temp = JSON.parse(localStorage.cartData);
      temp.push(el);
      localStorage.cartData = JSON.stringify(temp);
    }
  });
};

//Підказка при наведенні на кнопку "Save"
let toolTip = null;
document.querySelector(".people-cart-box").addEventListener("mouseover", (e) => {
  let input = e.target;
  if (input.tagName != "INPUT") return;
  toolTip = document.createElement("div");
  toolTip.innerText = "Збережеться в 'localStorage.cartData'";
  toolTip.classList.add("tooltip");
  document.body.append(toolTip);
  toolTip.style.left = e.clientX + 15 + "px";
  toolTip.style.top = e.clientY + 15 + "px";
  input.addEventListener("mouseleave", () => {
    toolTip.hidden = true;
  })
})

function show() {
  document.querySelector(".loader").style.display = "block";
}

// фунцикя для удаления индикатора загрузки.
function hide() {
  document.querySelector(".loader").style.display = "none";
}

window.addEventListener("DOMContentLoaded", show);

