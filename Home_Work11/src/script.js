// 1. Создай класс, который будет создавать пользователей с именем и фамилией.
//Добавить к классу метод для вывода имени и фамилии
const userName = document.querySelector("#name"),
  userSecondName = document.querySelector("#second-name"),
  showUser = document.querySelector("#show-user"),
  output = document.querySelector(".output");

function validateInput(e) {
  let input = e.target;
  if (/^[a-zа-я]+$/i.test(input.value)) {
    input.style.backgroundColor = "green";
  } else {
    input.style.backgroundColor = "red";
    return false;
  }
}

userName.addEventListener("change", validateInput);
userSecondName.addEventListener("change", validateInput);

let userArray = [];
let count = 0;
class User {
  createData() {
    console.log(userName.value + " " + userSecondName.value);
    this.name = userName.value;
    this.sName = userSecondName.value;
  }
  createUser() {
    let user = new User();
    userArray.push(user);
    count++;
  }
  showUser() {
    output.innerHTML += `${count}. ${this.name}  ${this.sName} <br>`;
  }
}

let user = new User();
showUser.addEventListener("click", () => {
  if (
    userName.style.backgroundColor == "green" &&
    userSecondName.style.backgroundColor == "green"
  ) {
    user.createData();
    user.createUser();
    user.showUser();
    userName.value = "";
    userSecondName.value = "";
    userName.style.backgroundColor = "";
    userSecondName.style.backgroundColor = "";
  } else alert("Input correct data");
});

// 2. Создай список состоящий из 4 листов. Используя джс обратись к 2 li
//и с использованием навигации по DOM дай 1 элементу синий фон, а 3 красный

let li2 = document.querySelector("ul>li:nth-child(2)");
li2.querySelector("ul>li:nth-child(1)").style.backgroundColor = "blue";
li2.querySelector("ul>li:nth-child(3)").style.backgroundColor = "red";

let ul = document.querySelector("#ul-color");
let changeColor = document.querySelector("#change-color");
ul.addEventListener("click", (e) => {
  let el = e.target;
  el.style.backgroundColor = changeColor.value;
});

// 3.Создай див высотой 400 пикселей и добавь на него событие наведения мышки.
//При наведении мышки выведите текстом координаты, где находится курсор мышки

const task3 = document.querySelector("div.task:nth-child(3)");
let field = document.createElement("div");
field.style.height = "400px";
field.style.width = "800px";
field.style.backgroundColor = "pink";
task3.append(field);

field.addEventListener("mousemove", (e) => {
  field.innerHTML = `Абсолютні координати: X:${e.pageX}-Y:${e.pageY} <br> Відносно вікна: X:${e.clientX}-Y:${e.clientY} <br> Відносто батьківського елемента: X:${e.offsetX}-Y:${e.offsetY} <br>`;
});

// 4. Создай кнопки 1,2,3,4 и при нажатии на кнопку выведи информацию о том
//какая кнопка была нажата
//Можна створити в JS

function createButton(value) {
  let btn = document.createElement("input");
  btn.type = "button";
  btn.value = `${value}`;
  btnBox.prepend(btn);
}

const btnBox = document.querySelector(".button-box");
btnBox.addEventListener("click", (e) => {
  let btn = e.target;
  if (btn.tagName != "INPUT") return;
  alert(`Натиснута кнопка ${btn.value}`);
});

//createButton("BTN1");
//createButton("BTN2");
//createButton("BTN3");
//createButton("BTN4");

// 5. Создай див и сделай так, чтобы при наведении на див,
//див изменял свое положение на странице
let div = null;
function createDiv(width, height, color, borderRadius) {
  div = document.createElement("div");
  div.style.cssText = `
  width: ${width}px;
  height: ${height}px;
  background-color: ${color};
  border-radius: ${borderRadius}px;
  transition: all 0.2s
`;
  document.querySelector("div.task:nth-child(5)").append(div);
}

createDiv(50, 50, "red", 30);

div.addEventListener("mouseover", (e) => {
  div.style.position = "absolute";
  let left = div.offsetLeft;
  let top = div.offsetTop;
  let newLeft = left;
  let newTop = top;
  newLeft = e.pageX + div.clientWidth;
  if (newLeft > document.body.clientWidth / 2) {
    newLeft = e.pageX - div.clientWidth*2;
  }
  newTop = e.pageY - div.clientHeight;
  div.style.left = newLeft + "px";
  div.style.top = newTop + "px";
});

// 6.Создай поле для ввода цвета, когда пользователь выберет какой-то
// цвет сделай его фоном body

const btnColor = document.querySelector("#btn-color");
const bodyColor = document.querySelector("#body-color");
btnColor.addEventListener("click", () => {
  document.body.style.backgroundColor = bodyColor.value;
})

// 7. Создай инпут для ввода логина, когда пользователь начнет Вводить данные
//в инпут выводи их в консоль

const userLogin = document.querySelector("#login");
const clearLogin = document.querySelector("#clear");

userLogin.addEventListener("keypress", (e) => {
  console.log(e.key);
})
clear.addEventListener("click", () => userLogin.value = "")


// 8.Создайте поле для ввода данных поле введения данных выведите текст
//под полем

let textField = document.createElement("input");
document.querySelector("div.task:nth-child(8)").append(textField);
textField.before("Введіть текст");
let info = document.createElement("div");
textField.after(info);
info.style.marginLeft = "100px";
textField.addEventListener("keypress", (e) => {
  info.innerText += e.key;
})
