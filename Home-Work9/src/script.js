//Task1 Stopwatch.
let [...spans] = document.querySelectorAll("span");
let [hour10, hour1, min10, min1, sec10, sec1] = spans;
let counter = 0;

const startBtn = document.querySelector("#start");
const stopBtn = document.querySelector("#stop");
const clearBtn = document.querySelector("#clear");

let startStopWatch = null;
function start() {
  startStopWatch = setInterval(() => {
    document.querySelector(".sec-box").style.backgroundColor = "green";
    ++counter;
    if (counter <= 9) {
      sec1.innerHTML = counter;
    }
    if (counter == 10) {
      counter = 0;
      sec1.innerHTML = counter;
      sec10.innerHTML = parseInt(sec10.innerHTML) + 1;
    }
    if (parseInt(sec10.innerHTML) == 6) {
      sec10.innerHTML = 0;
      min1.innerHTML = parseInt(min1.innerHTML) + 1;
    }
    if (parseInt(min1.innerHTML) == 10) {
      min1.innerHTML = 0;
      min10.innerHTML = parseInt(min10.innerHTML) + 1;
    }
    if (parseInt(min10.innerHTML) == 6) {
      min10.innerHTML = 0;
      hour1.innerHTML = parseInt(hour1.innerHTML) + 1;
    }
    if (parseInt(hour1.innerHTML) == 10) {
      hour1.innerHTML = 0;
      hour10.innerHTML = parseInt(min10.innerHTML) + 1;
    }
    if (hour1 == 4 && hour10 == 2) {
      clearField();
    }
  }, 1000);
  stopBtn.onclick = () => {
    clearInterval(startStopWatch);
    document.querySelector(".sec-box").style.backgroundColor = "red";
  };
  clearBtn.onclick = clearField;
}

function clearField() {
  clearInterval(startStopWatch);
  counter = 0;
  document.querySelector(".sec-box").style.backgroundColor = "grey";
  let [...spans] = document.querySelectorAll("span");
  spans.forEach((span) => (span.innerHTML = 0));
}
startBtn.onclick = start;

//Task2 Telephone test.

const pattern = /\b\d{3}-\d{3}-\d{2}-\d{2}\b/;
const tel = document.querySelector("#tel");
const save = document.querySelector("[value='Save']");
function testTel(value) {
  if (pattern.test(value)) {
    tel.style.backgroundColor = "green";
    setTimeout(() => location.href = "https://www.meme-arsenal.com/memes/c090127a787b24d3336a02d11bcd1428.jpg", 2000);
    document.querySelector(".error").innerHTML = "";
  } else {
    document.querySelector(".error").innerHTML = "Error, please input valid telephone!!!"
  }
};
save.onclick = () => testTel(tel.value);


//Task3 Slider

function querySelect(query) {
  return document.querySelector(query);
};
const previouseImg = querySelect(".previouse-img"),
nextImg = querySelect(".next-img"),
addImg = querySelect(".add-img"),
slider = querySelect(".slider-box");

const imgColection = [
  "https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg",
  "https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg",
  "https://img.tsn.ua/cached/371/tsn-8c5f6b23d1211bb14030cc3abd4583f7/thumbs/1200x630/91/2b/fe6881133a932e3ae6a34b60f8ee2b91.jpeg",
  "https://st.depositphotos.com/1546708/3658/i/600/depositphotos_36588689-stock-photo-venus.jpg"
]
slider.style.backgroundImage = `url(${imgColection[0]})`;
function add(path) {
  imgColection.push(path);
}

addImg.onclick = () => {
  let path = prompt("Введіть адресу зображення");
  add(path);
}


function next() {
  let img = slider.style.backgroundImage;
  imgColection.forEach((el, index, arr) => {
      if(img == `url("${el}")`) {
          if(index === arr.length -1) {
              index = -1;
          }
          slider.style.backgroundImage = `url(${imgColection[++index]})`; 
      }
  })
};
function previouse() {
  let img = slider.style.backgroundImage;
  console.log(img);
  imgColection.forEach((el, index, arr) => {
      if(img == `url("${el}")`) {
          if(index === 0) {
              index = arr.length;
          }
          slider.style.backgroundImage = `url(${imgColection[--index]})`; 
      }
  })
};
nextImg.onclick = next;
previouseImg.onclick = previouse;
window.onload = setInterval(next, 3000);