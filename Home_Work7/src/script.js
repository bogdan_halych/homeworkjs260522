// Реалізаія завдання з відгадування букв

class Games {
  constructor(world, attempts) {
    this.world = world.toUpperCase();
    this.attempts = attempts;
  }
  createArr() {
    let worldArr = Array.from(this.world);
    this.worldArr = worldArr;
  }
  createGuessesField() {
    let field = new Array(this.world.length);
    for (let i = 0; i < field.length; i++) {
      field[i] = "_";
    }
    this.field = Array.from(field);
  }
  startGames() {
    while (this.attempts != 0) {
      let letter = prompt("Input gusses letter").toUpperCase();
      if (this.worldArr.includes(letter)) {
        for (let i = 0; i < this.worldArr.length; i++) {
          if (this.worldArr[i] == letter) {
            this.field[i] = letter;
          }
        }
      }
      alert(`${this.field.join(" ")} ----- YOU have ${this.attempts - 1} attemption`);
      if (!this.field.includes("_")) {
        alert(`Congratulation!!! You WIN!!!`);
        return;
      }
      this.attempts--;
    }
    if (this.attempts == 0) {
      alert("The END");
    }
  }
}
let games = new Games(
  prompt("Input guesses world"),
  prompt("Input number of attemption")
);

games.createArr();

games.createGuessesField();

games.startGames(); 

//_____________________________________________________________________

function createNewUser(firstName, secondName) {
  return {
    firstName,
    secondName,
    getLogin() {
      let login = firstName.toLowerCase()[0] + secondName.toLowerCase();
      return login;
    }
  }
}
let newUser = createNewUser(prompt("Input first name"), prompt("Input second name"))
alert(newUser.firstName);
alert(newUser.secondName);
alert(newUser.getLogin());
console.log(newUser.firstName + " " + newUser.secondName + ", login: " + newUser.getLogin());

//Реалізація з використанням класів.

class User {
  constructor(firstName, secondName, birsday) {
    this.firstName = firstName;
    this.secondName = secondName;
    this.birsday = birsday;
  }
  getLogin() {
    let login = this.firstName.toLowerCase()[0] + this.secondName.toLowerCase();
    this.login = login;
  }
  getAge() {
    let year = this.birsday.split(".")[2];
    let month = this.birsday.split(".")[1];
    let day = this.birsday.split(".")[0];
    console.log(day + " " + month + " " + year);
    let date = new Date(year + "," + month + "," + day);
    console.log(date.getFullYear());
    let now = new Date();
    console.log(now.getFullYear());
    let age = now.getFullYear() - date.getFullYear();
    console.log(age)
    console.log(now.getMonth());
    console.log(date.getMonth());
    if(now.getMonth() < date.getMonth()) {
      age--;
    };
    if(now.getMonth() == date.getMonth()) {
      if(now.getDate() < date.getDate()) {
        age--;
      } else if(now.getDate() == date.getDate()) {
        alert("Happy Birthday!!!");
      }
    }
    this.age = age;
  }
  getPassword() {
    let password = this.firstName.toUpperCase()[0] + this.secondName.toLowerCase() + this.birsday.split(".")[2];
    this.password = password;
  }
}
let user = new User(prompt("Input first name"), prompt("Input second name"), prompt("Input age", "28.05.1983"));
user.getLogin();
alert("Login - " + user.login);
user.getAge();
alert("Age - " + user.age);
user.getPassword();
alert("Password - " + user.password);


//Завдання фільтрації за типом.

let myArr = [true, 10, "Bogdan", 10n, 102, 100, "Ukraine", "", {age: 100}];
function filterBy(arr, type) {
  return arr.filter((el) => type !== typeof el)
}

let res = filterBy(myArr, 'string');
alert(res);