function Human() {
  this.name = prompt("Input name");
  this.surname = prompt("Input surname");
  this.age = parseInt(prompt("Input age"));
}
const humansArray = [];
while (confirm("Do you want create new Human")) {
  humansArray.push(new Human());
}

//Створимо функцію, яка буде сортувати у порядку зростання значення Age:
function sortObjectByAgeDown(arr) {
  let newArray = [];
  let k = 0;
  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr.length; j++) {
      if (arr[i].age > arr[j].age) {
        k++;
      }
    }
    newArray[k] = arr[i];
    k = 0;
  }
  return newArray;
}
//Створимо функцію, яка буде сортувати у порядку зростання значення Age:
//Вона відрізняється тільки значенням в if().
function sortObjectByAgeUp(arr) {
  let newArray = [];
  let k = 0;
  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr.length; j++) {
      if (arr[i].age < arr[j].age) {
        k++;
      }
    }
    newArray[k] = arr[i];
    k = 0;
  }
  return newArray;
}

console.log(sortObjectByAgeDown(humansArray));
console.log(sortObjectByAgeUp(humansArray));


//Можна використати метод для масивів sort:
//Щоб отримати обернений - можна arr.reverse().
console.log(humansArray.sort((a, b) => a.age - b.age));
