//Створимо функцію, яка повертає об'єкт:
function createObject() {
  return {
    title: prompt("Input title", "Заголовок"),
    body: prompt("Input body", "Інформація в об'єкті"),
    footer: prompt("Input footer", "Кінець документа"),
    date: prompt("Input date", new Date())
  };
}
//Створюємо об'єкт:
let myMainDocument = createObject();
console.log(myMainDocument);

//Додаємо до об'єкта функцію, яка відображає його елементи:
myMainDocument.show = function () {
  document.querySelector(".title").innerHTML = this.title;
  document.querySelector(".body").innerHTML = this.body + "<br><hr><b>Дані з внутрішнього об'єкту<br>";
  document.querySelector(".footer").innerHTML = this.footer;
  document.querySelector(".date").innerHTML = this.date;
}

//Виводимо елементи на сторінку:
myMainDocument.show();

//Створюємо об'єкт в середині нашого об'єкту з допомогою createObject().
//Дані, які запитує prompt міняємо на власний розсуд 
myMainDocument.addition = createObject();
console.log(myMainDocument.addition);

for (let key in myMainDocument.addition) {
  document.querySelector(".body").innerHTML += myMainDocument.addition[key] + "<br>";
}

//У addition створюємо довільну кількість елементів:

//myMainDocument.addition.obj1 = createObject();
//myMainDocument.addition.obj2 = createObject();
myMainDocument.addition.obj3 = createObject();
console.log(myMainDocument.addition.obj3);

// Придумаємо функцію для виведення даних цих об'єктів у відповідному тегу:
function showObg(object, selector) {
  for (let key in object) {
    document.querySelector(selector).innerHTML += object[key] + "<br>";
  }
}
// Приклад для <div id:"test">:
showObg(myMainDocument.addition.obj3, "#test");
//document.querySelector("#test").innerHTML = 100;