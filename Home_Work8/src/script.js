let createCircle = document.querySelector("#circle");
window.onload = function() {
    createCircle.onclick = function() {
        let circleDiam = document.createElement("input");
        circleDiam.setAttribute("type", "text");
        circleDiam.value = 100;
        createCircle.after(circleDiam);
        let circlePaint = document.createElement("input");
        circlePaint.setAttribute("type", "button"); 
        circlePaint.setAttribute("value", "PAINT"); 
        circleDiam.after(circlePaint);
        if(circlePaint) {
            circlePaint.onclick = function() {
                document.body.innerHTML = "";
                let box = document.createElement("div");
                box.style.cssText = `
                margin: 20px;
                display: flex;
                flex-wrap: wrap;`;
                box.style.width = 10*circleDiam.value + 200 + "px"; 
                document.body.append(box);
                for(let i = 0; i < 100; i++) {
                    let circle = document.createElement("div");
                    circle.style.backgroundColor = getColor();
                    circle.style.width = circleDiam.value + "px";
                    circle.style.height = circleDiam.value + "px";
                    circle.style.marginLeft = "20px";
                    circle.style.marginTop = "20px";
                    circle.style.borderRadius = '50%';
                    box.append(circle); 
                }
                let [...circles] = document.querySelectorAll("body>div:nth-child(1)>div");
                console.dir(circles);
                for(let i = 0; i < circles.length; i++) {
                    circles[i].onclick = () => {
                        circles[i].style.display = "none";
                    }
                }
            }
        }
    }
    
}

function getColor() {
    return `hsl(${Math.floor(Math.random()*360)}, 50%, 50%)`;
}
