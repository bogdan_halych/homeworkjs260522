const add = (a, b) => Number(a) + Number(b);
const sub = (a, b) => Number(a) - Number(b);
const mul = (a, b) => Number(a) * Number(b);
const div = (a, b) => {
  if (b == "0") {
    calc.result = "E";
    return calc.result;
  }
  return a / b;
};

function res(a, b, sign) {
  switch (sign) {
    case "+":
      result = add(a, b);
      break;
    case "-":
      result = sub(a, b);
      break;
    case "*":
      result = mul(a, b);
      break;
    case "/":
      result = div(a, b);
      break;
  }
  return result;
}

const calc = {
  value1: "",
  value2: "",
  sign: "",
  result: "",
  member: "",
};

window.addEventListener("DOMContentLoaded", () => {
  const btn = document.querySelector(".keys"),
    display = document.querySelector(".display > input");
  let left = display.getBoundingClientRect().left;
  let top = display.getBoundingClientRect().top;
  console.log(left, top);

  function equal() {
    if (calc.value2 == "" && calc.sign == "") {
      calc.value2 = 1;
      calc.sign = "*";
    }
    calc.result = res(Number(calc.value1), Number(calc.value2), calc.sign);
    display.value = calc.result;
    calc.value1 = Number(calc.result);
    calc.value2 = "";
    calc.sign = "";
  }
  function clearCalc() {
    display.value = "";
    calc.value1 = "";
    calc.value2 = "";
    calc.result = "";
    calc.member = "";
    calc.sign = "";
  }
  function miniClear() {
    display.value = "";
    calc.result = "";
  }
  function createM() {
    let members = document.createElement("div");
    members.innerHTML = "m";
    document.body.append(members);
    members.classList.add("members");
    members.style.left = left + 5 + "px";
    members.style.top = top + 20 + "px";
    members.classList.add("hide");
  }
  createM();
  function showM() {
    let members = document.querySelector(".members");
    members.classList.remove("hide");
  }
  function hideM() {
    let members = document.querySelector(".members");
    members.classList.add("hide");
  }

  btn.addEventListener("click", function (e) {
    let el = e.target;
    if (el.tagName != "INPUT") return;
    if (/\d/.test(el.value) || /\./.test(el.value)) {
      if (display.value == calc.member && display.value != "") {
        miniClear();
      }
      if (
        display.value == calc.result &&
        calc.value2 == "" &&
        calc.sign == ""
      ) {
        calc.value1 = "";
        miniClear();
      }
      if (calc.value1 == "" || calc.sign === "") {
        if (/\./.test(calc.value1) && el.value === ".") return;
        calc.value1 += el.value;
        if (calc.value1[0] === ".") {
          calc.value1 = "0.";
        }
        display.value = Number(calc.value1);
      } else if (calc.sign !== "") {
        if (/\./.test(calc.value2) && el.value === ".") return;
        calc.value2 += el.value;
        if (calc.value2[0] === ".") {
          calc.value2 = "0.";
        }
        display.value = Number(calc.value2);
      }
    }
    if (el.classList.contains("pink")) {
      if (display.value == calc.member && display.value != "") {
        calc.value1 = Number(display.value);
        calc.value2 = "";
      }
      if (calc.sign != "" && calc.value2 != "") {
        equal();
      }
      calc.sign = el.value;
    }
    if (el.value === "=") {
      equal();
    }
    if (el.value === "C") {
      clearCalc();
      hideM();
    }

    if (el.value == "m+") {
      calc.member = Number(calc.member) + Number(display.value);
      equal();
      showM();
    }
    if (el.value == "m-") {
      equal();
      calc.member = Number(calc.member) - Number(display.value);
      console.log(calc.member);
      if (calc.member == "0") {
        hideM();
      }
    }
    if (el.value == "mrc") {
      display.value = Number(calc.member);
      if (calc.value1 !== "") {
        calc.value1 = Number(display.value);
      }
    }
    console.log(calc);
    console.log(display.value);
  });
});
