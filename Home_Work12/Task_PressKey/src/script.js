const [...buttons] = document.querySelectorAll(".btn-wrapper > .btn");
window.addEventListener("keypress", (e) => {
    let keyPressed = e.key[0].toUpperCase() + e.key.slice(1);
    buttons.forEach((el) => {
        if(el.innerText === keyPressed) {
            el.style.color = "blue";
      }
      if (el.innerText != keyPressed) {
        if (el.style.color === "blue") {
          el.style.color = "black";
        }
      }
    })
})