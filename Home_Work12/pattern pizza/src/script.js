// Створимо початкову вартість та ту, яка буде змінюватись
const startPizzaPrice = 50;
let pizzaPrice = startPizzaPrice;
const [...pizzaSize] = document.querySelectorAll(".radioIn"); //Масив radio

//Функція обчислення вартості в залежності від вибору величини
function firstCalc(id) {
  switch (id) {
    case "small": {
      pizzaPrice = startPizzaPrice + soucePrice + toppingPrice;
      break;
    }
    case "mid": {
      pizzaPrice = 1.5 * (startPizzaPrice + soucePrice + toppingPrice);
      break;
    }
    case "big": {
      pizzaPrice = 2 * (startPizzaPrice + soucePrice + toppingPrice);
      break;
    }
  }
}

let soucePrice = 0;
let toppingPrice = 0;
let fullPrice = 0;

//Встановимо ціни на соуси(однакові) і на топінги(однакові).
const [...ingridiends] = document.querySelectorAll(".ingridients img");
for (let i = 0; i < ingridiends.length; i++) {
  if (i < 3) {
    ingridiends[i].setAttribute("data-value", 10);
  } else ingridiends[i].setAttribute("data-value", 20);
}

//Підсумкові теги результату замовлення
const price = document.querySelector(".price>p");
const sauces = document.querySelector(".sauces>p");
const toppings = document.querySelector(".toppings>p");

function calc() {
  pizzaSize.forEach((el) => {
    if (el.checked == true) firstCalc(el.id);
  });
  let fullPrice = pizzaPrice;
  price.innerHTML = "Ціна: " + fullPrice + "грн.";
}

const changePizza = document.querySelector("#pizza");
changePizza.addEventListener("click", () => {
  calc();
  mainPizzaFieldImg.length = ingdids.length;
});

//Глобальні змінні для виконання
let moveEl = null;
let dragEl = null;
let droppableBelow = null;
let elemBelow = null;
const mainPizzaField = document.querySelector(".table");
let [...mainPizzaFieldImg] = document.querySelectorAll(".table>img");
let [...ingdids] = document.querySelectorAll(".table>img");

let changeForLost = null;

//Основна дія - перетягування шнгрідієнтів
document
  .querySelector(".ingridients")
  .addEventListener("mousedown", function (e) {
    [...mainPizzaFieldImg] = document.querySelectorAll(".table>img");
    [...ingdids] = document.querySelectorAll(".table>img");
    dragEl = e.target;
    if (dragEl.tagName != "IMG") return;
    dragEl.value = document.querySelector(`#${dragEl.id}+span`).innerText;
    moveEl = dragEl.cloneNode(true);
    moveEl.value = dragEl.value;
    let shiftLeft =
      e.clientX -
      document.querySelector(".ingridients").getBoundingClientRect().left;
    let shiftTop =
      e.clientY -
      document.querySelector(".ingridients").getBoundingClientRect().top;
    moveEl.style.position = "absolute";
    moveEl.style.zIndex = 10;
    document.body.append(moveEl);
    moveAt(e.pageX, e.pageY);
    function moveAt(pageX, pageY) {
      moveEl.style.left = pageX - shiftLeft + "px";
      moveEl.style.top = pageY - shiftTop + "px";
    }
    function onMouseMove(e) {
      moveAt(e.pageX, e.pageY);
      moveEl.hidden = true;
      elemBelow = document.elementFromPoint(e.clientX, e.clientY);
      moveEl.hidden = false;
    }
    document.addEventListener("mousemove", onMouseMove);
    document.addEventListener("mouseup", function () {
      document.removeEventListener("mousemove", onMouseMove);
      droppableBelow = elemBelow.closest(".table");
      if (droppableBelow) {
        moveEl.style.display = "inline-block";
        mainPizzaField.append(moveEl);
        moveEl.style.left = 0 + "px";
        moveEl.style.top = 0 + "px";
        [...ingdids] = document.querySelectorAll(".table>img");
        e.stopPropagation();
      } else {
        changeForLost = true;
      }
    });
  });
document.ondragstart = function () {
  return false;
};

let lastDragElement = null;
//Функція видалення img, якщо він не наклався на тіло піци. Подвійний клік
let dell = false;
document.addEventListener("dblclick", (e) => {
  dell = false;
  if (e.target.tagName === "IMG" && changeForLost == true) {
    let elementDell = e.target;
    if (elementDell.closest(".table")) return;
    elementDell.remove();
    let [...spans] = document.querySelectorAll(".result p>span");
    spans.forEach((element) => {
      let a = element.innerText;
      let b = getTextLastDrag().slice(1, 3);
      if (dell) return;
      if (a.includes(b)) {
        element.remove();
        dell = true;
      }
    });
  }
  changeForLost = false;
});

//Невелика інструкція по видаленню. Спрацьовує до 5 секунд, якщо елемент відпущено не в полі з класом "table"
let pToolTip = null;
function showHideToolTip() {
  if (changeForLost) {
    pToolTip = document.createElement("p");
    pToolTip.innerHTML =
      "Для видалення невдало вибраного елемента двічі клікніть по ньому";
    pToolTip.classList.add("tooltip");
    document.body.append(pToolTip);
    changeForLost = null;
    setTimeout(() => pToolTip.remove(), 4000);
  }
}
setInterval(showHideToolTip, 5000);

//Функція періодичної перевірки зміни ціни
function auditRes() {
  //if (dragEl.tagName != "IMG") return;
  if (ingdids.length > mainPizzaFieldImg.length) {
    if (moveEl.getAttribute("data-value") == 10) {
      soucePrice += Number(moveEl.getAttribute("data-value"));
    }
    if (moveEl.getAttribute("data-value") == 20) {
      toppingPrice += Number(moveEl.getAttribute("data-value"));
    }
    calc();
    mainPizzaFieldImg.length = ingdids.length;
  }
}
setInterval(() => auditRes(), 500);

window.onload = function () {
  let fullPrice = pizzaPrice;
  price.innerHTML = "Ціна: " + fullPrice + "грн.";
};

//Зберігаємо соуси і топпінги

function createList(elem) {
  if (dragEl) {
    let span = document.createElement("span");
    span.innerText = getText();
    elem.append(span);
    let b = document.createElement("br");
    span.append(b);
  }
  lastDragElement = dragEl;
  dragEl = null;
}

//Дістаєто текст з перенесеного елемента
function getText() {
  if (dragEl) {
    let id = dragEl.id;
    let spanText = document.querySelector(`#${id}+span`).innerHTML;
    return spanText;
  }
}

function getTextLastDrag() {
  if (lastDragElement) {
    let id = lastDragElement.id;
    let spanText = document.querySelector(`#${id}+span`).innerHTML;
    return spanText;
  }
}

function soucesAdd() {
  createList(sauces);
  dragEl = null;
}
function toppingAdd() {
  createList(toppings);
  dragEl = null;
}

setInterval(() => {
  if (dragEl && dragEl.getAttribute("data-value") == 10) {
    soucesAdd();
  }
  if (dragEl && dragEl.getAttribute("data-value") == 20) {
    toppingAdd();
  }
}, 500);

//Видалення інгрідієнтів при натисканні на них

const resultField = document.querySelector(".result");
let isDeleteElement = false;
resultField.addEventListener("click", (e) => {
  // [...mainPizzaFieldImg] = document.querySelectorAll(".table>img");
  // [...ingdids] = document.querySelectorAll(".table>img");
  isDeleteElement = false;
  let target = e.target;
  if (target.tagName != "SPAN") return;
  let [...tableField] = document.querySelectorAll(".table>img");
  let spanText = target.innerText;
  tableField.forEach((elem) => {
    if (isDeleteElement) return;
    if (spanText.includes(elem.value)) {
      if (elem.getAttribute("data-value") == 10) {
        soucePrice -= Number(elem.getAttribute("data-value"));
      }
      if (elem.getAttribute("data-value") == 20) {
        toppingPrice -= Number(elem.getAttribute("data-value"));
      }
      calc();
      elem.remove();
      target.remove();
      isDeleteElement = true;
      [...mainPizzaFieldImg] = document.querySelectorAll(".table>img");
      [...ingdids] = document.querySelectorAll(".table>img");
    }
  });
  auditDell();
});

function auditDell() {
  let [...elOfTable] = document.querySelectorAll(".table>img");
  let [...allSpan] = document.querySelectorAll(".result span");
  if(elOfTable.length != allSpan.length + 1) {
    elOfTable[elOfTable.length - 1].remove();
  }
}
// Перевіряємо форму

const validate = (target) => {
  switch (target.name) {
    case "name":
      return /^[A-zА-я_ ]{2,}$/i.test(target.value);
    case "phone":
      return /^\+380\d{9}$/.test(target.value);
    case "email":
      return /^[a-z._]+@[a-z._]+\.[a-z._]{1,4}$/i.test(target.value);
    default:
      throw new Error("Невірно введені дані");
  }
};

const info = document.forms.info;

class Order {
  constructor(name, phone, email) {
    this.name = name;
    this.phone = phone;
    this.email = email;
  }
  getData() {
    const [...pizzaSize] = document.querySelectorAll(".radioIn");
    const [...spans] = document.querySelectorAll(".result p>span");
    const pizza = {};
    pizzaSize.forEach((el) => {
      if (el.checked == true) {
        pizza.size = el.id;
      }
    });
    let ingridOrder = "";
    spans.forEach((el) => {
      if (el.innerText != "") {
        ingridOrder += el.innerText + ", ";
      }
    });
    this.pizza = pizza;
    this.ingridOrder = ingridOrder;
  }
}

let isValide = false;
info.addEventListener("click", (e) => {
  let target = e.target;
  if (target.name != "cancel" && target.name != "btnSubmit") {
    target.addEventListener("change", (e) => {
      isValide = validate(target);
      if (isValide) {
        target.style.backgroundColor = "green";
      } else target.style.backgroundColor = "red";
    });
  }
  if (target.name == "cancel") {
    let allInputText = [];
    allInputText.push(info.name);
    allInputText.push(info.phone);
    allInputText.push(info.email);
    allInputText.forEach((el) => (el.value = ""));
  }
  if (target.name == "btnSubmit") {
    if ((validate(info.name) && validate(info.phone), validate(info.email))) {
      let order = new Order(
        info.name.value,
        info.phone.value,
        info.email.value
      );
      order.getData();
      console.dir(order);
      alert("Створений Ордер в консолі");
    } else alert("Поля заповнені невірно або не заповнені взагалі");
  }
});

//Елемент, що тікає

const runElement = document.querySelector("#banner");
function bannerMove(element, pageX, pageY) {
  element.style.position = "absolute";
  document.body.append(runElement);
  let thisLeft = element.getBoundingClientRect().left;
  let thisTop = element.getBoundingClientRect().top;
  element.style.left = pageX - runElement.clientWidth + "px";
  element.style.top = pageY - runElement.clientHeight + "px";
  element.style.bottom = "auto";
  element.style.right = "auto";
  setTimeout(() => {
    element.style.left = thisLeft + "px";
    element.style.top = thisTop + "px";
  }, 3000);
}

runElement.addEventListener("mouseover", (e) => {
  let target = e.target;
  bannerMove(target, e.clientX, e.clientY);
});
