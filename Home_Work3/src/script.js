// Розв'яжемо цю задачу таким чином, щоб, коли кількість елементів масиву парна,
// то новий елемент просто вставлявся посередині.А, якщо непарна, - видаляв елемент
// посередині та вставляв на його місце новий.

const styles = ["Джаз", "Блюз"];
styles.push('Рок-н-рол');
document.write(styles.join(", ") + "<br><hr>")
let half = Math.floor(styles.length / 2);
if (styles.length % 2 == 0) {
  styles.splice(half, 0, "Класика")
} else {
  styles.splice(half, 1, "Класика")
}

document.write(styles.join(", ") + "<br><hr>")

styles.unshift('Реп', 'Реггі');
document.write('Так виглягає масив після добавлення у початок елементів Реп і Реггі - ' + styles.join(', ') + '<br><hr>');

