import React from "react";
import ReactDOM from "react-dom";
import "./main.css";

const DayWeek = () => {
  const days = ["Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота", "Неділя"];
  return (
    <div className="inner">
      <h3>Дні тижня</h3>
      <ol>
      {days.map((el, index) => <li key={index}>{el}</li>)};
      </ol>
    </div>
  );
};
const Month = () => {
  const months = ["Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень", "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"];
  return (
    <div className="inner">
      <h3>Місяці</h3>
      <ol>
      {months.map((el, index) => <li key={index}>{el}</li>)};
      </ol>
    </div>
  );
};
const Zodiac = () => {
  const zodiacs = ["Козеріг", "Водолій", "Риби", "Овен", "Тілець", "Близнюки", "Рак", "Лев", "Діва", "Терези", "Скорпіон", "Стрілець"];
  return (
    <div className="inner">
      <h3>Знаки зодіаку</h3>
      <ol>
      {zodiacs.map((el, index) => <li key={index}>{el}</li>)};
      </ol>
    </div>
  );
};

const App = () => {
  return (
    <div className="main">
      <DayWeek />
      <Month />
      <Zodiac />
    </div>
  );
};

ReactDOM.render(<App />, document.querySelector("#root"));
