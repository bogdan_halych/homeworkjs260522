// TASK 1

//Create array:
const arr = [];
for (let i = 0; i < 10; i++) {
  arr.push(Math.floor(Math.random() * 1000));
}

//Create even function:
function even(arr) {
  evenArr = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] % 2 === 0) {
      evenArr.push(arr[i]);
    }
    else continue;
  }
  return evenArr;
}


//Create odd function:
function odd(arr) {
  oddArr = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] % 2 === 1) {
      oddArr.push(arr[i]);
    }
    else continue;
  }
  return oddArr;
}


function map(fn, arr) {
  return fn(arr);
}

//За таким принципом можна необмежену кількість колбек функцій придумати.

alert(map(even, arr));
alert(map(odd, arr));



// TASK 2

function checkAge1(age) {
  return age >= 18 ? true : confirm("Батьки дозволили?")
}
alert(checkAge1(14));
alert(checkAge1(18));

function checkAge2(age) {
  return age =  age >= 18 || confirm("Батьки дозволили?")
}

alert(checkAge2(14));
alert(checkAge2(18));